const express = require('express');
const morgan = require('morgan');
const path = require('path');
const body_parser = require("body-parser");
const session = require('express-session');
const connect_flash = require('connect-flash');
const cookieParser = require('cookie-parser');
const mogoose = require('mongoose');





const home_router =require('./routers/home');
const auth_router =require('./routers/auth');
const admin_router =require('./routers/admin');
const speaker_router =require('./routers/speaker');
const event_router =require('./routers/event');





const app = express();
mogoose.connect('mongodb://localhost:27017/eventsDB');

//use morgan
app.use(morgan("short"));
//middle ware of time
/************************************ */
app.use((request, response, next)=>{
    let hours = (new Date()).getHours;
    let minute = (new Date()).getMinutes;
    if (true) {
        console.log('true');
        next();
        
    } else {
        console.log('error message');
        next(new Error('time out'));
    }

});
//**************************** */
app.use(express.static(path.join(__dirname,'public')));
app.use(express.static(path.join(__dirname,'node_modules','bootstrap','dist')));
app.use(express.static(path.join(__dirname,'node_modules','jquery','dist')));


app.use(body_parser.urlencoded({extended:false}));
app.use(body_parser.json());                                                                    
//session use
app.use(session({secret: "Shh, its a secret!"}));
//use connect-flash
app.use(connect_flash());
//use cookie
app.use(cookieParser());
//use multer
// app.use(multerMiddleWare);

//setting ejs 
app.set('view engine','ejs');
app.set('views',path.join(__dirname,"views"));

app.use(home_router);
app.use(auth_router);

//middle ware of session
app.use((request, response, next)=>{ 
    if(request.session.userName){
        response.locals.userName=request.session.userName;
        next()
    }else{
        request.flash('info','session ended ...........');
        response.redirect('/login');
    }

});



app.use('/speaker',speaker_router);
app.use('/admin',admin_router);
app.use('/events',event_router);





// app.use('/',(request, response, next)=>{
//     response.send('Home');
// });





/************************page not found */
app.use((req, res , next)=>{
    res.status(404).sendFile(path.join(__dirname,'views','404.html'));
});

/************error handel */
app.use((error,request,response,next)=>{
    response.send("Error "+error);
})


/******************** server listen */
app.listen(5000,()=>{
    console.log('server listen .......................');
    
});