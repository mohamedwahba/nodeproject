const express = require('express');
const path = require('path');
const admin_router = express.Router();

admin_router.get("/profile",(request,response)=>{
        if (request.session.role == 1) {
            response.render('admin/profile',{sum : request.cookies.sum});
        }else{
            response.redirect('/login');
        }


    // response.sendFile(path.join(__dirname,"..","views","admin","profile.html"));

});





module.exports = admin_router;
