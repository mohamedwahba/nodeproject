const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const fs =require('fs');

require("./../model/eventModel");
//load model of speaker
require('../model/speakerModel');

//use multer
const multer = require('multer');

let multerMiddleWare = multer({
    dest:"./public/images/"
}); 
const speaker_router = express.Router();

let speakerModel = mongoose.model('speakers');
let eventSchema = mongoose.model('events');





speaker_router.get("/profile", (request, response) => {
    // response.sendFile(path.join(__dirname,"..","views","speaker","profile.html"));
    if (request.session.role == 0) {
        
        let name = request.session.userName;
        speakerModel.findOne({name:name},(error,result)=>{
            if(!error)
            {
                // console.log("yhhhbgj"+result._id);                
            eventSchema.find({mainSpeaker:result._id})
                .populate({path:"mainSpeaker otherSpeakers"})
                .then((main_result)=>{ 
                        eventSchema.find({ otherSpeakers: result._id })
                        .populate({ path: "mainSpeaker otherSpeakers" })
                           .then((other_result) => {
                            response.render("speaker/profile", { main: main_result, other: other_result })
                            })
                            .catch((error) => {
                                console.log("other error" + error)
                            })

                })
                .catch((error)=>{console.log("profile error"+ error);
                });
            }

        })
        
    } else {
        response.redirect('/login');

    }
})
speaker_router.get("/list", (request, response) => {
    // response.render('speaker/speakersList');
    speakerModel.find({},(err, result)=>{

         if(!err){
            response.render('speaker/speakersList',{speakers:result});
         }else
         {
             console.log("Find All error" +error);
         }


    });


});

speaker_router.get("/add", (request, response) => {
    response.render('speaker/addSpeaker');


});
speaker_router.post("/add",multerMiddleWare.single("speakerImage"),(request, response) => {
    let speaker = new speakerModel({
        _id: request.body.id,
        name: request.body.name,
        image:request.file.originalname,

    });
    speaker.save((err, result) => {
        fs.rename(request.file.path,path.join(request.file.destination,request.file.originalname),(error) => {
            console.log("error"+error);
        });
        response.redirect('/speaker/list');
    });

});

speaker_router.get('/edit/:id',(request, response)=>{
    speakerModel.findOne({_id:request.params.id},(err, result)=>{
        response.render('speaker/editspeaker',{speakers:result});

    });

});

speaker_router.post('/edit/:id',multerMiddleWare.single("speakerImage"),(request, response)=>{
 fs.rename(request.file.path,path.join(request.file.destination,request.file.originalname),(error)=>{
    console.log("error",error);
    
 });
    speakerModel.update({_id:request.params.id},
        {"$set":{
            name:request.body.name,
            image:request.file.originalname
        }},
        (err, result)=>{
            response.redirect('/speaker/list');

    });

});
speaker_router.get("/delete/:id",(request,response)=>{
    speakerModel.remove({_id:request.params.id},(error)=>{
        if(!error)
        response.redirect("/speaker/list");
    })
});
speaker_router.get("/search/:value?",(request, response, next)=>{
    let arr = [ "mohamed","ahmed","hany","maged","hazem","fahmy","fayed" ];
    let value = request.params.value;
    let filterdArr = arr.filter(a => a.startsWith(value));
    response.send(JSON.stringify(filterdArr));
});


module.exports = speaker_router;
