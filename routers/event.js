const express = require('express');
const path = require('path');
const event_router = express.Router();
const mongoose=require("mongoose");


require('../model/eventModel');
require('../model/speakerModel');
let eventSchema= mongoose.model("events");
let speakerSchema=mongoose.model("speakers");


event_router.get("/add",(request,response)=>{
    // response.render('events/addevent');
    speakerSchema.find({},(error,result)=>{
        response.render("events/addevent",{speakers:result});
    });


});
event_router.post("/add",(request,response)=>{
    // response.render('events/addevent');
      //insert->eveent
      let event=new eventSchema({
        _id:request.body.id,
        title:request.body.title,
        mainSpeaker:request.body.mainSpeaker,
        otherSpeakers:request.body.otherSpeakers
    });

    event.save((error)=>{
        if(!error)
        // response.send("done");
        response.redirect("/events/list");
        else
        console.log("save event error "+error);
    });
    eventSchema.find({})
    .populate({path:"mainSpeaker otherSpeakers"})
    .then((result)=>{        
        response.render("events/eventsList",{events:result});
    })
    .catch((error)=>{});


});

event_router.get("/list",(request,response)=>{
    //  response.render('events/eventsList');
    eventSchema.find({})
    .populate({path:"mainSpeaker otherSpeakers"})
    .then((result)=>{
        response.render("events/eventsList",{events:result});
    })
    .catch((error)=>{
        console.log("list"+error);
        
    });
    
       
});
event_router.get('/edit/:id',(request, response)=>{   
    speakerSchema.find({},(err, resultall)=>{
        eventSchema.findOne({_id:request.params.id},(err, result)=>{
            console.log(result);
            // speakersarr = [result,resultall];
            response.render('events/editevent',{event:result,speakers:resultall});

        });

    });            
});
event_router.post("/edit/:id",(request,response)=>{

    eventSchema.updateOne({_id:request.params.id},{
        $set:{
            _id:request.body.id,
        title:request.body.title,
        mainSpeaker:request.body.mainSpeaker,
        otherSpeakers:request.body.otherSpeakers
        },
    },(error)=>{
        if(!error)
        {
            response.redirect("/events/list");
        }else{
            console.log("error in update "+error);
        }
    })

})





event_router.get("/delete/:id",(request,response)=>{
    eventSchema.remove({_id:request.params.id},(error)=>{
        if(!error)
        response.redirect("/events/list");
    })
});
module.exports = event_router;
