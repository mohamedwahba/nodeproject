const express = require('express');
const path = require('path');
const auth_router = express.Router();
require("../model/eventModel");
require("../model/speakerModel");
/***************login */

auth_router.get('/login',(request, response, next)=>{
    // response.sendFile(path.join(__dirname,"..","views","auth","login.html"));
    response.render('auth/login',{ messages: request.flash('info') });
})

auth_router.post('/login',(request, response, next)=>{
    console.log('body',request.body);
    let role = request.body.role;
    if(role==1)
    {   
        //admin
        if(request.body.userName=="mohamed"&&request.body.userPassword=="123")
        {

            if(request.cookies.sum){
                //get cookies
                let num = Number.parseInt(request.cookies.sum);
                //set cookie
                response.cookie('sum',num+1);
            }else{
                //set cookie
                response.cookie('sum',1);
            }
            
            request.session.userName=request.body.userName;
            request.session.role=role;

            //response.redirect("/speaker/add");
             response.redirect("/admin/profile");
        }
        else
        {
           request.flash('info','error in authontication');
           response.redirect("/login");
     
        }
    }else if(role==0){
      
        
        if(request.body.userName!=""&&request.body.userPassword!="")
        {
            console.log(request.body.userName);
            request.session.userName=request.body.userName;
            request.session.role=role;
            
            response.redirect("/speaker/profile");
        }
        else
        {
           request.flash('info','error in authontication');
           response.redirect("/login");
     
        }
    }





   

    

})
/************register */

auth_router.get('/register',(request, response, next)=>{
    // response.sendFile(path.join(__dirname,"..","views","auth","register.html"));
    response.render('auth/register');



})

auth_router.post('/register',(request, response, next)=>{
         console.log('body',request.body);
    
        //  response.redirect("/speaker/profile"+request.body.Name);
            response.send("Done");
    
    
 
    


})
/*******************log out */
auth_router.get('/logout',(request, response, next)=>{
    request.session.destroy(()=>{
    response.redirect('/login');      
    })

})

auth_router.use("/validate",(request, response)=>{
    console.log("77777777777777777777777777777777777777777777");
    console.log(request.body.userName);
    if (request.body.userName=="mohamed" && request.body.pass=="123") {
        response.send("TRUE");
        console.log("55555555555555555555555555555");
        
    }else{
        response.send("false");
    }
    
});

auth_router.use("/chat",(request,response)=>{
    console.log("chat");
    response.render("auth/chat");
    
});



module.exports = auth_router;