function ajax(query) {
    let text_box = document.getElementById("search").value;
    let ajaxRequest = new XMLHttpRequest();
    if (text_box == "") {
        document.getElementById("result").innerHTML = "";
    } else {
        ajaxRequest.open("GET", "/speaker/search/" +text_box);
        ajaxRequest.send();

        //listen
        ajaxRequest.onreadystatechange = function () {
            if (ajaxRequest.readyState == 4 && ajaxRequest.status == 200) {
                // document.getElementById("result").innerHTML = xmlhttp.responseText;
                    document.getElementById("result").innerHTML=JSON.parse(ajaxRequest.responseText);
            }
        }
    }


}