let mongoose=require("mongoose");
// let AutoIncrement = require('mongoose-sequence')(mongoose);

let eventSchema=new mongoose.Schema({
    _id:Number,
    title:String,
    mainSpeaker:{
        type:Number,
        ref:"speakers"
    },
    otherSpeakers:[
        {
            type:Number,
            ref:"speakers"
        } 
    ]
});
// eventSchema.plugin(AutoIncrement, {inc_field: '_id'});
mongoose.model("events",eventSchema);

